core = 6.x

api = 2

; Pressflow core
projects[pressflow][download][url] = "https://github.com/pressflow/6/archive/pressflow-6.28.111.zip" 
projects[pressflow][download][type] = "get"
projects[pressflow][type] = "core"
projects[pressflow][patch][901534] = "http://drupal.org/files/drupal-taxonomy_notice-901534-2.patch"
projects[pressflow][patch][905156] = "http://drupal.org/files/issues/form_notice_1.patch"

; Libraries
libraries[jquery][download][url] = "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"
libraries[jquery][download][type] = get
libraries[jquery][destination] = "modules/contrib/jquery_ui"
libraries[jquery][directory_name] = "jquery.ui"

libraries[simplepie][download][url] = "https://raw.github.com/simplepie/simplepie/one-dot-two/simplepie.inc"
libraries[simplepie][download][type] = "get"
libraries[simplepie][destination] = "modules/contrib/feeds"
libraries[simplepie][directory_name] = "libraries"

libraries[solrphp][download][url] = "https://solr-php-client.googlecode.com/files/SolrPhpClient.r22.2009-11-09.tgz"
libraries[solrphp][download][type] = "get"
libraries[solrphp][destination] = "modules/contrib/apachesolr"
libraries[solrphp][directory_name] = "SolrPhpClient"

libraries[swfupload1][download][url] = "https://swfupload.googlecode.com/svn/swfupload/tags/swfupload_v2.2.0_core/Flash/swfupload.swf"
libraries[swfupload1][download][type] = "get"
libraries[swfupload1][directory_name] = "swfupload"

libraries[swfupload2][download][url] = "https://swfupload.googlecode.com/svn/swfupload/tags/swfupload_v2.2.0_core/swfupload.js"
libraries[swfupload2][download][type] = "get"
libraries[swfupload2][directory_name] = "swfupload"

;unpacked by drush make, see issue 1788908
;libraries[tika][download][url] = "http://archive.apache.org/dist/tika/tika-app-0.10.jar"
;libraries[tika][download][type] = "get"
;libraries[tika][directory_name] = "tika-app"

libraries[tinymce][download][url] = "https://github.com/downloads/tinymce/tinymce/tinymce_3.5.8.zip"
libraries[tinymce][download][type] = "get"


; Contrib modules
projects[admin_language][subdir] = "contrib"
projects[admin_language][version] = "1.4"
projects[admin_language][patch][1985812] = "http://drupal.org/files/admin_language-weight-1985812-1.patch"

projects[apachesolr][subdir] = "contrib"
projects[apachesolr][version] = "2.0-beta2"
;projects[apachesolr][directory_name] = "apachesolr"
;projects[apachesolr][download][type] = "get"
;projects[apachesolr][download][url] = "http://drupalcode.org/project/apachesolr.git/snapshot/f97a7f5.tar.gz"
projects[apachesolr][patch][1985886] = "http://drupal.org/files/apachesolr-date-1985886-1.patch"
projects[apachesolr][patch][1985934] = "http://drupal.org/files/apachesolr-datestamp-1985934-2.patch"
projects[apachesolr][patch][815350] = "http://drupal.org/files/issues/apachesolr_date_2.patch"
projects[apachesolr][patch][1985990] = "http://drupal.org/files/apachesolr-date_facet_solr36-1985990-1.patch"
projects[apachesolr][patch][1988292] = "https://drupal.org/files/apachesolr-notice-1988292-1.patch"

projects[apachesolr_attachments][subdir] = "contrib"
projects[apachesolr_attachments][version] = "2.0-alpha3"

projects[advagg][subdir] = "contrib"
projects[advagg][version] = "1.9"

projects[advpoll][subdir] = "contrib"
projects[advpoll][directory_name] = "advpoll"
projects[advpoll][download][type] = "get"
projects[advpoll][download][url] = "http://drupalcode.org/project/advpoll.git/snapshot/8e37d67.tar.gz"
projects[advpoll][patch][1985848] = "http://drupal.org/files/advpoll-noactivepoll-1985848-2.patch"

projects[autoload][subdir] = "contrib"
projects[autoload][version] = "2.1"

projects[backup_migrate][subdir] = "contrib"
projects[backup_migrate][version] = "2.7"

projects[beididp][subdir] = "contrib"
projects[beididp][version] = "1.0-alpha7"

projects[better_formats][subdir] = "contrib"
projects[better_formats][version] = "1.2"
projects[better_formats][patch][1988290] = "http://drupal.org/files/better_formats-notice-1988290-1.patch"

projects[cacheexclude][subdir] = "contrib"
projects[cacheexclude][version] = "2.2"

projects[cck][subdir] = "contrib"
projects[cck][version] = "2.9"

projects[cd][subdir] = "contrib"
projects[cd][version] = "1.6"

projects[config_perms][subdir] = "contrib"
projects[config_perms][version] = "1.0"

projects[content_profile][subdir] = "contrib"
projects[content_profile][version] = "1.0"
projects[content_profile][patch][803320] = "http://drupal.org/files/issues/content_profile_ds_0.patch"

projects[content_taxonomy][subdir] = "contrib"
projects[content_taxonomy][version] = "1.0-rc2"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.10"

projects[custom_formatters][subdir] = "contrib"
projects[custom_formatters][version] = "1.6"

projects[date][subdir] = "contrib"
projects[date][version] = "2.9"

projects[diff][subdir] = "contrib"
projects[diff][version] = "2.3"

projects[ds][subdir] = "contrib"
projects[ds][version] = "1.4"

projects[emf][subdir] ="contrib"
projects[emf][version] = "1.4"

projects[emfield][subdir] = "contrib"
projects[emfield][version] = "2.6"

projects[faq][subdir] = "contrib"
projects[faq][version] = "1.13"

projects[fbconnect][subdir] = "contrib"
projects[fbconnect][version] = "2.0-beta1"

projects[feeds][subdir] = "contrib"
projects[feeds][version] = "1.0-beta13"

projects[feeds_xpathparser][subdir] = "contrib"
projects[feeds_xpathparser][version] = "1.12"

projects[filefield][subdir] = "contrib"
projects[filefield][version] = "3.11"

projects[fivestar][subdir] = "contrib"
projects[fivestar][version] = "1.20"

projects[flag][subdir] = "contrib"
projects[flag][version] = "1.3"
projects[flag][patch][1986048] = "http://drupal.org/files/flag-fadeout-1986048-1.patch"
projects[flag][patch][882344] = "http://drupal.org/files/issues/flag_notice.patch"

projects[fontyourface][subdir] = "contrib"
projects[fontyourface][version] = "2.10"

projects[globalredirect][subdir] = "contrib"
projects[globalredirect][version] = "1.5"

projects[gmap][subdir] = "contrib"
projects[gmap][version] = "1.1"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "3.5"

projects[gvs][subdir] = "contrib"
projects[gvs][version] = "2.4"

projects[hansel][subdir] = "contrib"
projects[hansel][version] = "1.3"

projects[hierarchical_select][subdir] = "contrib"
projects[hierarchical_select][version] = "3.8"

;1.4 cannot be found on drupal.org
;projects[i18nviews][subdir] = "contrib/i18n"
;projects[i18nviews][version] = "2.0"
 
projects[i18n][subdir] = "contrib"
projects[i18n][version] = "1.5"
projects[i18n][patch][1986028] = "http://drupal.org/files/i18n-conimbo-1986028-1.patch"
projects[i18n][patch][1986034] = "http://drupal.org/files/i18n-conimbo_one-1986034-6.patch"
projects[i18n][patch][1989074] = "http://drupal.org/files/i18n-notice-1989074-2.patch"

projects[ife][subdir] = "contrib"
projects[ife][version] = "1.3"

projects[imageapi][subdir] = "contrib"
projects[imageapi][version] = "1.10"

projects[imagecache][subdir] = "contrib"
projects[imagecache][version] = "2.0-rc1"

projects[imagefield][subdir] = "contrib"
projects[imagefield][version] = "3.11"

projects[job_scheduler][subdir] = "contrib"
projects[job_scheduler][version] = "1.0-beta3"

projects[jqp][subdir] = "contrib"
projects[jqp][version] = "2.5"

projects[jquery_ui][subdir] = "contrib"
projects[jquery_ui][version] = "1.5"

projects[l10n_client][subdir] = "contrib"
projects[l10n_client][version] = "1.8"

projects[less][subdir] = "contrib"
projects[less][version] = "2.8"

projects[lightbox2][subdir] = "contrib"
projects[lightbox2][version] = "1.11"

projects[link][subdir] = "contrib"
projects[link][version] = "2.10"

projects[linkchecker][subdir] = "contrib"
projects[linkchecker][version] = "2.7"

projects[linkit_picker][subdir] = "contrib"
projects[linkit_picker][version] = "1.x-dev"

projects[linkit][subdir] = "contrib"
projects[linkit][version] = "1.12"
projects[linkit][patch][1986852] = "http://drupal.org/files/linkit-autocomplete_language-1986852-1.patch"

projects[location][subdir] = "contrib"
projects[location][version] = "3.2"

projects[logintoboggan][subdir] = "contrib"
projects[logintoboggan][version] = "1.10"

projects[media_vimeo][subdir] = "contrib"
projects[media_vimeo][version] = "1.1"

projects[media_youtube][subdir] = "contrib"
projects[media_youtube][version] = "1.3"

projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.4"

projects[menu_per_role][subdir] = "contrib"
projects[menu_per_role][version] = "1.11"

projects[mollom][subdir] = "contrib"
projects[mollom][version] = "2.5"

projects[nd][subdir] = "contrib"
projects[nd][version] = "2.4"

projects[nd_contrib][subdir] = "contrib"
projects[nd_contrib][version] = "2.4"

;almost impossible to figure out which version is used
;latest version(s) break the site because of incompatible contrimbo/plugins
;projects[nice_dash][subdir] = "contrib"
;projects[nice_dash][download][type] = "get" 
;projects[nice_dash][download][url] = "http://drupalcode.org/project/nice_dash.git/snapshot/f945502.tar.gz"

projects[nodeaccess][subdir] = "contrib"
projects[nodeaccess][version] = "1.3"

projects[nodeformcols][subdir] = "contrib"
projects[nodeformcols][version] = "1.6"
projects[nodeformcols][patch][664840] = "http://drupal.org/files/issues/nodeformcols_undefined_index_type.patch"

projects[nodepicker][subdir] = "contrib"
projects[nodepicker][version] = "1.0-alpha4"

projects[nodequeue][subdir] = "contrib"
projects[nodequeue][version] = "2.11"

projects[nodewords][subdir] = "contrib"
projects[nodewords][version] = "1.14"

projects[onepageprofile][subdir] = "contrib"
projects[onepageprofile][version] = "1.13"

projects[options_element][subdir] = "contrib"
projects[options_element][version] = "1.8"

projects[page_title][subdir] = "contrib"
projects[page_title][version] = "2.3"
projects[page_title][patch][1987270] = "http://drupal.org/files/page_title-default-1987270-1.patch"
projects[page_title][patch][1989826] = "http://drupal.org/files/page_title-notice-1989826-1.patch"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.10"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.6"

projects[pathfilter][subdir] = "contrib"
projects[pathfilter][version] = "1.0"

projects[pearwiki_filter][subdir] = "contrib"
projects[pearwiki_filter][version] = "1.0-beta1"

projects[protect_critical_users][subdir] = "contrib"
projects[protect_critical_users][version] = "1.1"

projects[recaptcha][subdir] = "contrib"
projects[recaptcha][version] = "1.7"

projects[quiz][subdir] = "contrib"
projects[quiz][version] = "4.4"

projects[robotstxt][subdir] = "contrib"
projects[robotstxt][version] = "1.3"

projects[scheduler][subdir] = "contrib"
projects[scheduler][version] = "1.9"

projects[seckit][subdir] = "contrib"
projects[seckit][version] = "1.6"

projects[securepages_prevent_hijack][subdir] = "contrib"
projects[securepages_prevent_hijack][version] = "1.6"

projects[securepages][subdir] = "contrib"
projects[securepages][version] = "1.9"

projects[securesite][subdir] = "contrib"
projects[securesite][version] = "2.4"

projects[site_map][subdir] = "contrib"
projects[site_map][version] = "1.2"
projects[site_map][patch][1987280] = "http://drupal.org/files/site_map-taxonomy-1987280-2.patch"
projects[site_map][patch][1987284] = "http://drupal.org/files/site_map-css-1987284-1.patch"

; directory_name doesn't seem to work on modules
projects[social-share][subdir] = "contrib"
;projects[social-share][version] = "1.12"
projects[social-share][download][type] = "get"
projects[social-share][download][url] = "http://ftp.drupal.org/files/projects/social-share-6.x-1.12.zip"
projects[social-share][directory_name] = "social_share"

projects[sweaver][subdir] = "contrib"
projects[sweaver][download][type] = "get"
projects[sweaver][download][url] = "http://drupalcode.org/project/sweaver.git/snapshot/a095825.tar.gz"
projects[sweaver][patch][1989920] = "http://drupal.org/files/sweaver-allowed_properties-1989920-1.patch"

projects[swfupload][subdir] = "contrib"
projects[swfupload][version] = "2.0-beta8"

projects[tableofcontents][subdir] = "contrib"
projects[tableofcontents][version] = "3.8"

projects[tagadelic][subdir] = "contrib"
projects[tagadelic][version] = "1.3"

projects[tcontact][subdir]= "contrib"
projects[tcontact][version] = "1.2"

projects[token][subdir] = "contrib"
projects[token][version] = "1.19"

projects[translation_overview][subdir] = "contrib"
projects[translation_overview][version] = "2.4"
projects[translation_overview][patch][1987292] = "http://drupal.org/files/translation_overview-dbrewrite-1987292-1.patch"

projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.1"

projects[tweetmeme][subdir] = "contrib"
projects[tweetmeme][version] = "1.3"

projects[twitter][subdir] = "contrib"
projects[twitter][version] = "2.6"

projects[ud][subdir] = "contrib"
projects[ud][version] = "1.3"

projects[username_enumeration_prevention][subdir] = "contrib"
projects[username_enumeration_prevention][version] = "1.0"

projects[varnish][subdir] = "contrib"
projects[varnish][version] = "1.2"

projects[vd][subdir] = "contrib"
projects[vd][version] = "1.3"

projects[views][subdir] = "contrib"
projects[views][version] = "2.16"

projects[views_bonus][subdir] = "contrib"
projects[views_bonus][version] = "1.1"

; mixed with inc files from actions subdir
;projects[views_bulk_operations][subdir] = "contrib"
;projects[views_bulk_operations][version] = "1.12"

projects[viewsdisplaytabs][subdir] = "contrib"
projects[viewsdisplaytabs][version] = "1.0-beta6"
projects[viewsdisplaytabs][patch][760352] = "http://drupal.org/files/issues/viewsdisplaystabs_notices.patch"
projects[viewsdisplaytabs][patch][760352] = "http://drupal.org/files/issues/viewsdisplaystabs_notices_1.patch"
projects[viewsdisplaytabs][patch][596446] = "http://drupal.org/files/issues/viewsdisplaytabs-596446.patch"
projects[viewsdisplaytabs][patch][1989946] = "http://drupal.org/files/viewsdisplaytabs-notices-1989946-1.patch"

projects[views_slideshow][subdir] = "contrib"
projects[views_slideshow][version] = "2.4"

projects[votingapi][subdir] = "contrib"
projects[votingapi][version] = "2.3"

projects[webform][subdir] = "contrib"
projects[webform][version] = "3.19"

projects[wikitools][subdir] = "contrib"
projects[wikitools][version] = "1.3"

projects[workflow][subdir] = "contrib"
projects[workflow][version] = "1.5"
projects[workflow][patch][1987746] = "http://drupal.org/files/workflow-css-1987746-1.patch"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.4"

projects[xml_parser][subdir] = "contrib"
projects[xml_parser][version] = "1.0"

projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][version] = "2.0-rc2"
projects[xmlsitemap][patch][1987770] = "http://drupal.org/files/xmlsitemap-default-1987770-1.patch"

; Contrimbo modules
projects[dc_storm][subdir] = "contrimbo"
projects[dc_storm][version] = "1.0-rc1"

projects[mmw_addemar][subdir] = "contrimbo"
projects[mmw_addemar][version] = "0.3"

projects[mmw_agenda][subdir] = "contrimbo"
projects[mmw_agenda][version] = "0.2"

projects[mmw_panels][subdir] = "contrimbo"
projects[mmw_panels][version] = "0.3"

projects[mmw_totop][subdir] = "contrimbo"
projects[mmw_totop][version] = "0.2"

projects[poll_override][subdir] = "contrimbo"
projects[poll_override][version] = "0.x-dev"


; Development modules
projects[coder][subdir] = "development"
projects[coder][version] = "2.0-rc1"

projects[devel][subdir] = "development"
projects[devel][version] = "1.27"

projects[devel_themer][subdir] = "development"
projects[devel_themer][version] = "1.x-dev"

projects[taxonomy_export][subdir] = "development"
projects[taxonomy_export][version] = "1.3"

